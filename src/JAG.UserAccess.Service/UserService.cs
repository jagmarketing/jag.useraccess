﻿using JAG.UserAccess.Contract;
using JAG.UserAccess.Dto;
using log4net;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Security.Claims;
using System.ServiceModel;

namespace JAG.UserAccess.Service
{
    [ServiceBehavior]
    public class UserService : IUserService
    {
        private static readonly ILog _Log = LogManager.GetLogger(typeof(UserService));

        [OperationBehavior]
        public string TestService()
        {
            try
            {
                string versionString = Assembly.GetExecutingAssembly().GetName().Version.ToString();

                return $"Success {versionString}";
            }
            catch (Exception exception)
            {
                _Log.Error($"An error occurred while testing logging service. {exception.Message}", exception);
            }

            return null;
        }

        [OperationBehavior]
        public List<ApplicationUserDto> FetchApplicationUsers(Guid tenantId, List<string> roles)
        {
            List<ApplicationUserDto> response = new List<ApplicationUserDto>();

            return response;
        }

        [OperationBehavior]
        public ClaimsIdentity Login(ApplicationUserDto userDto)
        {
            ClaimsIdentity response = null;

            return response;
        }

        [OperationBehavior]
        public string FetchEmail(ApplicationUserDto userDto)
        {
            string response = null;

            return response;
        }
    }
}