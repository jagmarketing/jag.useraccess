﻿using JAG.UserAccess.Dto;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.ServiceModel;

namespace JAG.UserAccess.Contract
{
    [ServiceContract]
    public interface IUserService
    {
        /// <summary>
        /// Make a test call to the service
        /// </summary>
        /// <returns></returns>
        [OperationContract()]
        string TestService();

        /// <summary>
        /// Fetch all users who are a part of the role(s) passed in.
        /// </summary>
        [OperationContract]
        List<ApplicationUserDto> FetchApplicationUsers(Guid tenantId, List<string> roles);

        /// <summary>
        /// Expose a function to validate users against the SpearHead database.
        /// </summary>
        [OperationContract]
        ClaimsIdentity Login(ApplicationUserDto userDto);

        /// <summary>
        /// Expose a function to validate users against the SpearHead database.
        /// </summary>
        [OperationContract]
        string FetchEmail(ApplicationUserDto userDto);
    }
}