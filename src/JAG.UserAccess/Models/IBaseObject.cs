﻿using System;

namespace JAG.UserAccess.Models
{
    public interface IBaseObject
    {
        Guid Id { get; set; }
    }
}