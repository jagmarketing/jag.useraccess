﻿using System;
using System.Runtime.Serialization;

namespace JAG.UserAccess.Models
{
    public class ApplicationUserDto : IBaseObject
    {
        [DataMember]
        public Guid Id { get; set; }

        [DataMember]
        public string Username { get; set; }

        [DataMember]
        public string Password { get; set; }

        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string LastName { get; set; }

        [DataMember]
        public string EmailAddress { get; set; }
    }
}