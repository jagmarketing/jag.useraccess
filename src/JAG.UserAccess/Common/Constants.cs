﻿using System;
using System.Linq;

namespace JAG.UserAccess.Common
{
    public class Constants
    {
        public const string ApiKey = "apikey";

        /// <summary>
        /// Formats name, upper case first character and lowers remaining characters.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string Formatted(string name)
        {
            if (String.IsNullOrWhiteSpace(name))
            {
                return String.Empty;
            }

            //Only if the name is all lower case or all upper case.
            if (name.All(a => Char.IsUpper(a)) || name.All(a => Char.IsLower(a)))
            {
                name = $"{name.Substring(0, 1).ToUpper()}{(name.Length > 1 ? name.Substring(1, name.Length - 1).ToLower() : String.Empty)}";
            }

            return name;
        }
    }
}