﻿using JAG.Common.DataTransfer.Response;
using JAG.Common.Services;
using JAG.UserAccess.Dto;
using JAG.UserAccess.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Cors;
using System.Web.Mvc;

namespace JAG.UserAccess.Controllers
{
    [Authorize]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class AccountController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public AccountController() { }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult UserLogin(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View("Login", model);
            }

            UserResponseDto user = Login(new UserDto() { Password = model.Password, UserName = model.Username }, false);

            if (user != null && user.Success)
            {
                return RedirectToLocal(returnUrl);
            }

            model.Message = user?.Message;

            return View("Login", model);
        }

        /// <summary>
        /// Accept raw string content and serializes the raw data to a specific object.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login()
        {
            UserDto applicationUser = new UserDto();
            UserResponseDto userResponse = new UserResponseDto();
            string content = String.Empty;

            try
            {
                //Guard against unauthorized requests.
                bool apiKeyIsValid = Request != null &&
                                     Request.Headers != null &&
                                     Request.Headers.GetValues(Common.Constants.ApiKey) != null &&
                                     Request.Headers.GetValues(Common.Constants.ApiKey).FirstOrDefault() == ConfigurationManager.AppSettings.Get(Common.Constants.ApiKey) ? true : false;

                //Return bad request if the api key is not valid.
                if (!apiKeyIsValid)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
                }

                content = new StreamReader(Request.InputStream).ReadToEnd();

                if (String.IsNullOrWhiteSpace(content))
                {
                    userResponse.Message = "Request body content is empty.";
                }
                else
                {
                    applicationUser = JsonConvert.DeserializeObject<UserDto>(content);

                    if (applicationUser == null ||
                        String.IsNullOrWhiteSpace(applicationUser.Password) ||
                        String.IsNullOrWhiteSpace(applicationUser.UserName))
                    {
                        userResponse.Message = $"Login failed for user name {applicationUser?.UserName}.";
                    }
                    else
                    {
                        userResponse = Login(applicationUser, true);
                    }
                }

                HttpContext.Response.Headers.Add(Common.Constants.ApiKey, Request.Headers.GetValues(Common.Constants.ApiKey).FirstOrDefault());
            }
            catch (Exception exception)
            {
                userResponse.Message = $"Error occurred while logging in user {applicationUser?.UserName}. {exception.Message} Content: {content}";
            }

            return new ContentResult
            {
                Content = JsonConvert.SerializeObject(userResponse),
                ContentType = "application/json",
                ContentEncoding = Encoding.UTF8
            };
        }

        private UserResponseDto Login(UserDto applicationUser, bool isApiRequest)
        {
            Response<ClaimsIdentity> response = new Response<ClaimsIdentity>();

            UserResponseDto userResponse = new UserResponseDto()
            {
                UserName = applicationUser.UserName
            };

            try
            {
                if (!isApiRequest)
                {
                    AuthenticationManager.SignOut();
                }

                Models.ApplicationUserDto user = new Models.ApplicationUserDto()
                {
                    Username = applicationUser.UserName,
                    Password = applicationUser.Password
                };

                response = PostRequest<Response<ClaimsIdentity>>("UserInternal/Login", user);

                if (response.Success)
                {
                    Guid applicationUserId = Guid.Parse(response.Data.FindFirst(ClaimTypes.NameIdentifier).Value);

                    List<string> userRoles = response.Data.Claims
                                                     .Where(ci => ci.Type == ClaimTypes.Role)
                                                     .Select(ci => ci.Value)
                                                     .ToList();

                    userResponse.Message = "Success";
                    userResponse.Success = true;
                    userResponse.UserRoles = userRoles;
                    userResponse.UserId = applicationUserId;
                    userResponse.Name = response.Data.Name;

                    if (!isApiRequest)
                    {
                        AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = true }, response.Data);
                    }
                }
                else
                {
                    userResponse.Message = $"Login failed for user name {applicationUser?.UserName}.";
                }
            }
            catch (Exception exception)
            {
                userResponse.Message = $"Login failed for user name {applicationUser?.UserName}. {exception.Message}";
            }

            return userResponse;
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        [AllowAnonymous]
        public ActionResult ExternalLoginCallback(string returnUrl)
        {
            if (AuthenticationManager != null &&
                AuthenticationManager.User != null &&
                AuthenticationManager.User.Identity != null &&
                AuthenticationManager.User.Identity.IsAuthenticated)
            {
                AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = true }, AuthenticationManager.User.Identities.First());

                return RedirectToLocal(returnUrl);
            }

            ViewBag.ReturnUrl = returnUrl;

            return RedirectToAction("Login");
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Manage");
            }

            if (ModelState.IsValid)
            {
                //Get the information about the user from the external login provider
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                        return RedirectToLocal(returnUrl);
                    }
                }

                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;

            return View(model);
        }

        [HttpPost]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);

            return RedirectToAction("Login", "Account");
        }

        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }

        #region Helpers

        //Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }

            return RedirectToAction("Login", "Account");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }

        public static T PostRequest<T>(string uri, object data)
        {
            var response = default(T);

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(ConfigurationManager.AppSettings.Get("LeadSystemInternalAPI"));
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var responseMessage = client.PostAsJsonAsync(uri, data).Result;

                if (responseMessage.IsSuccessStatusCode)
                {
                    response = responseMessage.Content.ReadAsAsync<T>().Result;
                }
            }

            return response;
        }

        #endregion
    }
}