﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(JAG.UserAccess.Startup))]
namespace JAG.UserAccess
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}