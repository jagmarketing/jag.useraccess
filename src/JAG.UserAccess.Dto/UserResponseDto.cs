﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace JAG.UserAccess.Dto
{
    public class UserResponseDto
    {
        [JsonProperty("success")]
        public bool Success { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("userId")]
        public Guid? UserId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("userName")]
        public string UserName { get; set; }

        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }

        [JsonProperty("userRoles")]
        public List<string> UserRoles { get; set; }
    }
}