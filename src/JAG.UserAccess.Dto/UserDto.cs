﻿using Newtonsoft.Json;

namespace JAG.UserAccess.Dto
{
    public class UserDto
    {
        [JsonProperty("userName")]
        public string UserName { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }
    }
}