﻿using System;

namespace JAG.UserAccess.Dto
{
    public interface IBaseObject
    {
        Guid Id { get; set; }
    }
}